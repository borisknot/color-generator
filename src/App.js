import { useCallback, useEffect, useRef, useState } from 'react';
import styles from './App.module.css';

export default function App() {
  const [colors, setColors] = useState([]);
  const numberOfColors = useRef();
  const LIMIT = {
    MIN: 1,
    MAX: 10,
  };

  const generateColors = useCallback(() => {
    const colors = Math.max(Math.min(Number(numberOfColors.current.value), LIMIT.MAX), LIMIT.MIN);
    numberOfColors.current.value = colors;
    const generatedColors = Array.from(Array(colors)).map(() => generateColor());
    setColors(generatedColors);
  }, [LIMIT.MIN, LIMIT.MAX]);

  useEffect(() => {
    generateColors();
  }, [generateColors]);

  const generateColor = () => {
    const RGB_MAX_VALUE = (256 * 256 * 256);
    return Math.floor(Math.random() * RGB_MAX_VALUE)
      .toString(16)
      .padStart(6, '0')
      .padStart(7, '#');
  }

  const renderColor = (color, index) => {
    return (
      <div
        key={index}
        className={styles.Color}
        style={{backgroundColor: color}}>
        <span className={styles.ColorText}>{color}</span>
      </div>
    );
  }

  return (
    <div className={styles.App}>
      <div className={styles.ControlsHolder}>
        <div className={styles.Controls}>
          <label htmlFor="number-of-colors">Number of colors</label>
          <input
            type="number"
            id="number-of-colors"
            defaultValue="3"
            min={LIMIT.MIN}
            max={LIMIT.MAX}
            ref={numberOfColors}
            className={styles.NumberOfColors}
          />
          <button onClick={generateColors}>Go!</button>
        </div>
      </div>
      <div className={styles.Generator}>
        {colors.map((color, index) => renderColor(color, index))}
      </div>
    </div>
  );
}
